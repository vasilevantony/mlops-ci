FROM mambaorg/micromamba
USER root

WORKDIR /app

COPY environment.yml /app/environment.yml
RUN micromamba create -f environment.yml

COPY pyproject.toml /app/pyproject.toml
COPY poetry.lock /app/poetry.lock
RUN micromamba run -n dev poetry install

COPY ./docs/_quarto.yml /app/docs/_quarto.yml
COPY ./docs/index.qmd /app/docs/index.qmd
COPY ./docs/trees_data_analyzing.ipynb /app/docs/trees_data_analyzing.ipynb
COPY ./docs/about.qmd /app/docs/about.qmd
COPY ./docs/styles.css /app/docs/styles.css

COPY mlops/app.py /app/mlops/app.py
# ENTRYPOINT [ "micromamba", "run", "-n", "dev", "python", "test.py" ]  # v1 - run python script
# ENTRYPOINT [ "micromamba", "run", "-n", "dev", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root" ]  # v2 - run jupyter notebook
